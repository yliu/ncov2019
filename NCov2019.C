TGraph* incRate (const TGraph* gr)
{
  const int MAXNEL = 100;
  Double_t *x_origin = gr->GetX();
  Double_t *y_origin = gr->GetY();
  const int n_origin = gr->GetN();
  Double_t x[MAXNEL] = {0};
  Double_t y[MAXNEL] = {0};
  for(int i=10; i!=n_origin-1;++i)
    {
      x[i-10] =  x_origin[i+1];
      y[i-10] = (y_origin[i+1] - y_origin[i])/y_origin[i]; //  skip the first 10 days
      
    }
  return new TGraph(n_origin-1-10,x,y);
}
int NCov2019()
{
  //  gStyle->SetOptTitle(1);
  TLatex* tx = new TLatex();
  const int MAXNEL = 100;
  double ncasesCountry[] = {278,326,547,639,916,2000,2700,4400,6000,7711,9692,11794,14380,17205,20438,24324,28018,31161,34546,37198,40171,42638,44653,59804,63851};
  double ncasesHubeiNoWH[] = {0,67,124,55,157,434,725,1124,1649,2325,3167,3938,4965,6035,7138,8327,9548,10494,11350,12118,12729,13274,13808,15212,15995};
  double ncasesWuhan[]= {258,308,320,459,572,618,698,1590,1905,2261,2639,3215,4109,5142,6384,8351,10117,11618,13608,14982,16902,18454,19558,32994,35991};
  int nel = sizeof(ncasesWuhan)/sizeof(double);
  double ncasesChinaNoHubei[MAXNEL]={0};
  double x[MAXNEL] = {0};
  for(int i=0; i!=nel; ++i)
    {
      ncasesChinaNoHubei[i] = ncasesCountry[i] - ncasesWuhan[i] - ncasesHubeiNoWH[i];
      x[i] = double(i);
    }
  TCanvas *c1 = new TCanvas();
  TGraph* gwh=0;
  TGraph* ghb=0;
  TGraph* gel=0;
  TGraph* gto=0;
  c1->Divide(2,2);
  c1->cd(1); gwh = new TGraph(nel,x,ncasesWuhan); gwh->SetMarkerStyle(20);gwh->Draw("ALP"); gwh->SetTitle("WuHan");tx->DrawLatexNDC(0.3,0.7,"Wuhan");
  c1->cd(2); ghb = new TGraph(nel,x,ncasesHubeiNoWH); ghb->SetMarkerStyle(20);ghb->Draw("ALP");ghb->SetTitle("HuBei - WuHan");tx->DrawLatexNDC(0.3,0.7,"Hubei - Wuhan");
  c1->cd(3); gel = new TGraph(nel,x,ncasesChinaNoHubei);gel->SetMarkerStyle(20);gel->Draw("ALP");gel->SetTitle("China-HuBei"); tx->DrawLatexNDC(0.3,0.7,"China - Hubei");
  c1->cd(4); gto = new TGraph(nel,x,ncasesCountry); gto->SetMarkerStyle(20);gto->Draw("ALP");gto->SetTitle("China"); tx->DrawLatexNDC(0.3,0.7,"China");
  TGraph* p[4] = {gwh,ghb,gel,gto};
  for(int j=0;j!=4;++j)
    {
      p[j]->GetXaxis()->SetTitle("Days since Jan 20");
      p[j]->GetXaxis()->CenterTitle();
      p[j]->GetYaxis()->SetTitle("# of confirmed cases");
      p[j]->GetYaxis()->CenterTitle();
      
    }

  
  TCanvas *c2 = new TCanvas();
  TGraph* dgwh = incRate(gwh);//dgwh->Draw("ALP"); 
  
  TGraph* dghb = incRate(ghb);//dghb->Draw("ALP");
  
  TGraph* dgel = incRate(gel); // dgel->Draw("ALP");
 
  TGraph* dgto = incRate(gto); //dgto->Draw("ALP");
  
  c2->Divide(2,2);
  c2->cd(1); dgwh->SetMarkerStyle(20);dgwh->Draw("ALP"); dgwh->SetTitle("WuHan");tx->DrawLatexNDC(0.6,0.7,"Wuhan");
  c2->cd(2); dghb->SetMarkerStyle(20);dghb->Draw("ALP"); dghb->SetTitle("HuBei - WuHan");tx->DrawLatexNDC(0.6,0.7,"Hubei - Wuhan");
  c2->cd(3); dgel->SetMarkerStyle(20);dgel->Draw("ALP");  dgel->SetTitle("China-HuBei");tx->DrawLatexNDC(0.6,0.7,"China - Hubei");
  c2->cd(4); dgto->SetMarkerStyle(20);dgto->Draw("ALP");  dgto->SetTitle("China"); tx->DrawLatexNDC(0.6,0.7,"China");
  TGraph* dp[4] = {dgwh,dghb,dgel,dgto};
  for(int j=0;j!=4;++j)
    {
      dp[j]->GetXaxis()->SetTitle("Days since Jan 20");
      dp[j]->GetYaxis()->SetTitle("increase rate");
      dp[j]->GetXaxis()->CenterTitle();
      dp[j]->GetYaxis()->CenterTitle();
      
    }
  
  

  
  return 0;
}
